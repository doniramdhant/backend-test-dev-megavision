from django.contrib import admin
from django.urls import path, include #new

urlpatterns = [
    path('admin/', admin.site.urls),
    path('global/', include('global.urls')), #new
    path('dashboard/', include('dashboard.urls')) #new
]

