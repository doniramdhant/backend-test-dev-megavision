# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AUsers(models.Model):
    users_id = models.AutoField(db_column='USERS_ID', primary_key=True)  # Field name made lowercase.
    username = models.CharField(db_column='USERNAME', max_length=100, blank=True, null=True)  # Field name made lowercase.
    password = models.CharField(db_column='PASSWORD', max_length=100, blank=True, null=True)  # Field name made lowercase.
    users_status = models.CharField(db_column='USERS_STATUS', max_length=100, blank=True, null=True)  # Field name made lowercase.
    group_id = models.CharField(db_column='GROUP_ID', max_length=10, blank=True, null=True)  # Field name made lowercase.
    nama = models.CharField(db_column='NAMA', max_length=200, blank=True, null=True)  # Field name made lowercase.
    printer_url = models.CharField(db_column='PRINTER_URL', max_length=25, blank=True, null=True)  # Field name made lowercase.
    api_keys = models.CharField(db_column='API_KEYS', max_length=25, blank=True, null=True)  # Field name made lowercase.
    i_area1 = models.CharField(db_column='I_AREA1', max_length=2, blank=True, null=True)  # Field name made lowercase.
    i_area2 = models.CharField(db_column='I_AREA2', max_length=2, blank=True, null=True)  # Field name made lowercase.
    i_area3 = models.CharField(db_column='I_AREA3', max_length=2, blank=True, null=True)  # Field name made lowercase.
    i_area4 = models.CharField(db_column='I_AREA4', max_length=2, blank=True, null=True)  # Field name made lowercase.
    i_area5 = models.CharField(db_column='I_AREA5', max_length=2, blank=True, null=True)  # Field name made lowercase.
    f_menu_all = models.BooleanField(db_column='F_MENU_ALL', blank=True, null=True)  # Field name made lowercase.
    f_menu0 = models.BooleanField(db_column='F_MENU0', blank=True, null=True)  # Field name made lowercase.
    f_menu1 = models.BooleanField(db_column='F_MENU1', blank=True, null=True)  # Field name made lowercase.
    f_menu2 = models.BooleanField(db_column='F_MENU2', blank=True, null=True)  # Field name made lowercase.
    f_menu3 = models.BooleanField(db_column='F_MENU3', blank=True, null=True)  # Field name made lowercase.
    f_menu4 = models.BooleanField(db_column='F_MENU4', blank=True, null=True)  # Field name made lowercase.
    f_menu5 = models.BooleanField(db_column='F_MENU5', blank=True, null=True)  # Field name made lowercase.
    f_menu6 = models.BooleanField(db_column='F_MENU6', blank=True, null=True)  # Field name made lowercase.
    f_menu7 = models.BooleanField(db_column='F_MENU7', blank=True, null=True)  # Field name made lowercase.
    f_menu8 = models.BooleanField(db_column='F_MENU8', blank=True, null=True)  # Field name made lowercase.
    f_menu9 = models.BooleanField(db_column='F_MENU9', blank=True, null=True)  # Field name made lowercase.
    f_menu10 = models.BooleanField(db_column='F_MENU10', blank=True, null=True)  # Field name made lowercase.
    f_menu11 = models.BooleanField(db_column='F_MENU11', blank=True, null=True)  # Field name made lowercase.
    f_menu12 = models.BooleanField(db_column='F_MENU12', blank=True, null=True)  # Field name made lowercase.
    f_menu13 = models.BooleanField(db_column='F_MENU13', blank=True, null=True)  # Field name made lowercase.
    f_menu14 = models.BooleanField(db_column='F_MENU14', blank=True, null=True)  # Field name made lowercase.
    f_menu15 = models.BooleanField(db_column='F_MENU15', blank=True, null=True)  # Field name made lowercase.
    f_menu16 = models.BooleanField(db_column='F_MENU16', blank=True, null=True)  # Field name made lowercase.
    f_menu17 = models.BooleanField(db_column='F_MENU17', blank=True, null=True)  # Field name made lowercase.
    f_menu18 = models.BooleanField(db_column='F_MENU18', blank=True, null=True)  # Field name made lowercase.
    f_menu19 = models.BooleanField(db_column='F_MENU19', blank=True, null=True)  # Field name made lowercase.
    f_menu20 = models.BooleanField(db_column='F_MENU20', blank=True, null=True)  # Field name made lowercase.
    f_menu21 = models.BooleanField(db_column='F_MENU21', blank=True, null=True)  # Field name made lowercase.
    f_menu22 = models.BooleanField(db_column='F_MENU22', blank=True, null=True)  # Field name made lowercase.
    f_menu23 = models.BooleanField(db_column='F_MENU23', blank=True, null=True)  # Field name made lowercase.
    f_menu24 = models.BooleanField(db_column='F_MENU24', blank=True, null=True)  # Field name made lowercase.
    f_menu25 = models.BooleanField(db_column='F_MENU25', blank=True, null=True)  # Field name made lowercase.
    f_menu26 = models.BooleanField(db_column='F_MENU26', blank=True, null=True)  # Field name made lowercase.
    f_menu27 = models.BooleanField(db_column='F_MENU27', blank=True, null=True)  # Field name made lowercase.
    f_menu28 = models.BooleanField(db_column='F_MENU28', blank=True, null=True)  # Field name made lowercase.
    f_menu29 = models.BooleanField(db_column='F_MENU29', blank=True, null=True)  # Field name made lowercase.
    f_menu30 = models.BooleanField(db_column='F_MENU30', blank=True, null=True)  # Field name made lowercase.
    f_menu31 = models.BooleanField(db_column='F_MENU31', blank=True, null=True)  # Field name made lowercase.
    f_menu32 = models.BooleanField(db_column='F_MENU32', blank=True, null=True)  # Field name made lowercase.
    f_menu33 = models.BooleanField(db_column='F_MENU33', blank=True, null=True)  # Field name made lowercase.
    f_menu34 = models.BooleanField(db_column='F_MENU34', blank=True, null=True)  # Field name made lowercase.
    f_menu35 = models.BooleanField(db_column='F_MENU35', blank=True, null=True)  # Field name made lowercase.
    f_menu36 = models.BooleanField(db_column='F_MENU36', blank=True, null=True)  # Field name made lowercase.
    f_menu37 = models.BooleanField(db_column='F_MENU37', blank=True, null=True)  # Field name made lowercase.
    f_menu38 = models.BooleanField(db_column='F_MENU38', blank=True, null=True)  # Field name made lowercase.
    f_menu39 = models.BooleanField(db_column='F_MENU39', blank=True, null=True)  # Field name made lowercase.
    f_menu40 = models.BooleanField(db_column='F_MENU40', blank=True, null=True)  # Field name made lowercase.
    f_menu41 = models.BooleanField(db_column='F_MENU41', blank=True, null=True)  # Field name made lowercase.
    f_menu42 = models.BooleanField(db_column='F_MENU42', blank=True, null=True)  # Field name made lowercase.
    f_menu43 = models.BooleanField(db_column='F_MENU43', blank=True, null=True)  # Field name made lowercase.
    f_menu44 = models.BooleanField(db_column='F_MENU44', blank=True, null=True)  # Field name made lowercase.
    f_menu45 = models.BooleanField(db_column='F_MENU45', blank=True, null=True)  # Field name made lowercase.
    f_menu46 = models.BooleanField(db_column='F_MENU46', blank=True, null=True)  # Field name made lowercase.
    f_menu47 = models.BooleanField(db_column='F_MENU47', blank=True, null=True)  # Field name made lowercase.
    f_menu48 = models.BooleanField(db_column='F_MENU48', blank=True, null=True)  # Field name made lowercase.
    f_menu49 = models.BooleanField(db_column='F_MENU49', blank=True, null=True)  # Field name made lowercase.
    f_menu50 = models.BooleanField(db_column='F_MENU50', blank=True, null=True)  # Field name made lowercase.
    f_menu51 = models.BooleanField(db_column='F_MENU51', blank=True, null=True)  # Field name made lowercase.
    f_menu52 = models.BooleanField(db_column='F_MENU52', blank=True, null=True)  # Field name made lowercase.
    f_menu53 = models.BooleanField(db_column='F_MENU53', blank=True, null=True)  # Field name made lowercase.
    f_menu54 = models.BooleanField(db_column='F_MENU54', blank=True, null=True)  # Field name made lowercase.
    f_menu55 = models.BooleanField(db_column='F_MENU55', blank=True, null=True)  # Field name made lowercase.
    f_menu56 = models.BooleanField(db_column='F_MENU56', blank=True, null=True)  # Field name made lowercase.
    f_menu57 = models.BooleanField(db_column='F_MENU57', blank=True, null=True)  # Field name made lowercase.
    f_menu58 = models.BooleanField(db_column='F_MENU58', blank=True, null=True)  # Field name made lowercase.
    f_menu59 = models.BooleanField(db_column='F_MENU59', blank=True, null=True)  # Field name made lowercase.
    f_menu60 = models.BooleanField(db_column='F_MENU60', blank=True, null=True)  # Field name made lowercase.
    f_menu61 = models.BooleanField(db_column='F_MENU61', blank=True, null=True)  # Field name made lowercase.
    f_menu62 = models.BooleanField(db_column='F_MENU62', blank=True, null=True)  # Field name made lowercase.
    f_menu63 = models.BooleanField(db_column='F_MENU63', blank=True, null=True)  # Field name made lowercase.
    f_menu64 = models.BooleanField(db_column='F_MENU64', blank=True, null=True)  # Field name made lowercase.
    f_menu65 = models.BooleanField(db_column='F_MENU65', blank=True, null=True)  # Field name made lowercase.
    f_menu66 = models.BooleanField(db_column='F_MENU66', blank=True, null=True)  # Field name made lowercase.
    f_menu67 = models.BooleanField(db_column='F_MENU67', blank=True, null=True)  # Field name made lowercase.
    f_menu68 = models.BooleanField(db_column='F_MENU68', blank=True, null=True)  # Field name made lowercase.
    f_menu69 = models.BooleanField(db_column='F_MENU69', blank=True, null=True)  # Field name made lowercase.
    f_menu70 = models.BooleanField(db_column='F_MENU70', blank=True, null=True)  # Field name made lowercase.
    f_menu71 = models.BooleanField(db_column='F_MENU71', blank=True, null=True)  # Field name made lowercase.
    f_menu72 = models.BooleanField(db_column='F_MENU72', blank=True, null=True)  # Field name made lowercase.
    f_menu73 = models.BooleanField(db_column='F_MENU73', blank=True, null=True)  # Field name made lowercase.
    f_menu74 = models.BooleanField(db_column='F_MENU74', blank=True, null=True)  # Field name made lowercase.
    f_menu75 = models.BooleanField(db_column='F_MENU75', blank=True, null=True)  # Field name made lowercase.
    f_menu76 = models.BooleanField(db_column='F_MENU76', blank=True, null=True)  # Field name made lowercase.
    f_menu77 = models.BooleanField(db_column='F_MENU77', blank=True, null=True)  # Field name made lowercase.
    f_menu78 = models.BooleanField(db_column='F_MENU78', blank=True, null=True)  # Field name made lowercase.
    f_menu79 = models.BooleanField(db_column='F_MENU79', blank=True, null=True)  # Field name made lowercase.
    f_menu80 = models.BooleanField(db_column='F_MENU80', blank=True, null=True)  # Field name made lowercase.
    f_menu81 = models.BooleanField(db_column='F_MENU81', blank=True, null=True)  # Field name made lowercase.
    f_menu82 = models.BooleanField(db_column='F_MENU82', blank=True, null=True)  # Field name made lowercase.
    f_menu83 = models.BooleanField(db_column='F_MENU83', blank=True, null=True)  # Field name made lowercase.
    f_menu84 = models.BooleanField(db_column='F_MENU84', blank=True, null=True)  # Field name made lowercase.
    f_menu85 = models.BooleanField(db_column='F_MENU85', blank=True, null=True)  # Field name made lowercase.
    f_menu86 = models.BooleanField(db_column='F_MENU86', blank=True, null=True)  # Field name made lowercase.
    f_menu87 = models.BooleanField(db_column='F_MENU87', blank=True, null=True)  # Field name made lowercase.
    f_menu88 = models.BooleanField(db_column='F_MENU88', blank=True, null=True)  # Field name made lowercase.
    f_menu89 = models.BooleanField(db_column='F_MENU89', blank=True, null=True)  # Field name made lowercase.
    f_menu90 = models.BooleanField(db_column='F_MENU90', blank=True, null=True)  # Field name made lowercase.
    f_menu91 = models.BooleanField(db_column='F_MENU91', blank=True, null=True)  # Field name made lowercase.
    f_menu92 = models.BooleanField(db_column='F_MENU92', blank=True, null=True)  # Field name made lowercase.
    f_menu93 = models.BooleanField(db_column='F_MENU93', blank=True, null=True)  # Field name made lowercase.
    f_menu94 = models.BooleanField(db_column='F_MENU94', blank=True, null=True)  # Field name made lowercase.
    f_menu95 = models.BooleanField(db_column='F_MENU95', blank=True, null=True)  # Field name made lowercase.
    f_menu96 = models.BooleanField(db_column='F_MENU96', blank=True, null=True)  # Field name made lowercase.
    f_menu97 = models.BooleanField(db_column='F_MENU97', blank=True, null=True)  # Field name made lowercase.
    f_menu98 = models.BooleanField(db_column='F_MENU98', blank=True, null=True)  # Field name made lowercase.
    f_menu99 = models.BooleanField(db_column='F_MENU99', blank=True, null=True)  # Field name made lowercase.
    f_menu100 = models.BooleanField(db_column='F_MENU100', blank=True, null=True)  # Field name made lowercase.
    f_aktif = models.BooleanField(db_column='F_AKTIF', blank=True, null=True)  # Field name made lowercase.
    f_special = models.BooleanField(db_column='F_SPECIAL', blank=True, null=True)  # Field name made lowercase.
    i_nik = models.CharField(db_column='I_NIK', max_length=6, blank=True, null=True)  # Field name made lowercase.
    f_menu101 = models.BooleanField(db_column='F_MENU101', blank=True, null=True)  # Field name made lowercase.
    f_menu102 = models.BooleanField(db_column='F_MENU102', blank=True, null=True)  # Field name made lowercase.
    f_menu103 = models.BooleanField(db_column='F_MENU103', blank=True, null=True)  # Field name made lowercase.
    f_menu104 = models.BooleanField(db_column='F_MENU104', blank=True, null=True)  # Field name made lowercase.
    f_menu105 = models.BooleanField(db_column='F_MENU105', blank=True, null=True)  # Field name made lowercase.
    f_menu106 = models.BooleanField(db_column='F_MENU106', blank=True, null=True)  # Field name made lowercase.
    f_menu107 = models.BooleanField(db_column='F_MENU107', blank=True, null=True)  # Field name made lowercase.
    f_menu108 = models.BooleanField(db_column='F_MENU108', blank=True, null=True)  # Field name made lowercase.
    f_menu109 = models.BooleanField(db_column='F_MENU109', blank=True, null=True)  # Field name made lowercase.
    f_menu110 = models.BooleanField(db_column='F_MENU110', blank=True, null=True)  # Field name made lowercase.
    f_menu111 = models.BooleanField(db_column='F_MENU111', blank=True, null=True)  # Field name made lowercase.
    f_menu112 = models.BooleanField(db_column='F_MENU112', blank=True, null=True)  # Field name made lowercase.
    f_menu113 = models.BooleanField(db_column='F_MENU113', blank=True, null=True)  # Field name made lowercase.
    f_menu114 = models.BooleanField(db_column='F_MENU114', blank=True, null=True)  # Field name made lowercase.
    f_menu115 = models.BooleanField(db_column='F_MENU115', blank=True, null=True)  # Field name made lowercase.
    f_menu116 = models.BooleanField(db_column='F_MENU116', blank=True, null=True)  # Field name made lowercase.
    f_menu117 = models.BooleanField(db_column='F_MENU117', blank=True, null=True)  # Field name made lowercase.
    f_menu118 = models.BooleanField(db_column='F_MENU118', blank=True, null=True)  # Field name made lowercase.
    f_menu119 = models.BooleanField(db_column='F_MENU119', blank=True, null=True)  # Field name made lowercase.
    f_menu120 = models.BooleanField(db_column='F_MENU120', blank=True, null=True)  # Field name made lowercase.
    f_menu121 = models.BooleanField(db_column='F_MENU121', blank=True, null=True)  # Field name made lowercase.
    f_menu122 = models.BooleanField(db_column='F_MENU122', blank=True, null=True)  # Field name made lowercase.
    f_menu123 = models.BooleanField(db_column='F_MENU123', blank=True, null=True)  # Field name made lowercase.
    f_menu124 = models.BooleanField(db_column='F_MENU124', blank=True, null=True)  # Field name made lowercase.
    f_menu125 = models.BooleanField(db_column='F_MENU125', blank=True, null=True)  # Field name made lowercase.
    f_menu126 = models.BooleanField(db_column='F_MENU126', blank=True, null=True)  # Field name made lowercase.
    f_menu127 = models.BooleanField(db_column='F_MENU127', blank=True, null=True)  # Field name made lowercase.
    f_menu128 = models.BooleanField(db_column='F_MENU128', blank=True, null=True)  # Field name made lowercase.
    f_menu129 = models.BooleanField(db_column='F_MENU129', blank=True, null=True)  # Field name made lowercase.
    f_menu130 = models.BooleanField(db_column='F_MENU130', blank=True, null=True)  # Field name made lowercase.
    group_id_1 = models.CharField(db_column='GROUP_ID_1', max_length=10, blank=True, null=True)  # Field name made lowercase.
    group_id_2 = models.CharField(db_column='GROUP_ID_2', max_length=10, blank=True, null=True)  # Field name made lowercase.
    group_id_3 = models.CharField(db_column='GROUP_ID_3', max_length=10, blank=True, null=True)  # Field name made lowercase.
    group_id_4 = models.CharField(db_column='GROUP_ID_4', max_length=10, blank=True, null=True)  # Field name made lowercase.
    group_id_5 = models.CharField(db_column='GROUP_ID_5', max_length=10, blank=True, null=True)  # Field name made lowercase.
    group_id_6 = models.CharField(db_column='GROUP_ID_6', max_length=10, blank=True, null=True)  # Field name made lowercase.
    group_id_7 = models.CharField(db_column='GROUP_ID_7', max_length=10, blank=True, null=True)  # Field name made lowercase.
    group_id_8 = models.CharField(db_column='GROUP_ID_8', max_length=10, blank=True, null=True)  # Field name made lowercase.
    group_id_9 = models.CharField(db_column='GROUP_ID_9', max_length=10, blank=True, null=True)  # Field name made lowercase.
    group_id_10 = models.CharField(db_column='GROUP_ID_10', max_length=10, blank=True, null=True)  # Field name made lowercase.
    level = models.CharField(db_column='LEVEL', max_length=10, blank=True, null=True)  # Field name made lowercase.
    f_menu131 = models.BooleanField(db_column='F_MENU131', blank=True, null=True)  # Field name made lowercase.
    f_menu132 = models.BooleanField(db_column='F_MENU132', blank=True, null=True)  # Field name made lowercase.
    f_menu133 = models.BooleanField(db_column='F_MENU133', blank=True, null=True)  # Field name made lowercase.
    f_menu134 = models.BooleanField(db_column='F_MENU134', blank=True, null=True)  # Field name made lowercase.
    f_menu135 = models.BooleanField(db_column='F_MENU135', blank=True, null=True)  # Field name made lowercase.
    f_menu136 = models.BooleanField(db_column='F_MENU136', blank=True, null=True)  # Field name made lowercase.
    f_menu137 = models.BooleanField(db_column='F_MENU137', blank=True, null=True)  # Field name made lowercase.
    f_menu138 = models.BooleanField(db_column='F_MENU138', blank=True, null=True)  # Field name made lowercase.
    f_menu139 = models.BooleanField(db_column='F_MENU139', blank=True, null=True)  # Field name made lowercase.
    f_menu140 = models.BooleanField(db_column='F_MENU140', blank=True, null=True)  # Field name made lowercase.
    f_menu141 = models.BooleanField(db_column='F_MENU141', blank=True, null=True)  # Field name made lowercase.
    f_menu142 = models.BooleanField(db_column='F_MENU142', blank=True, null=True)  # Field name made lowercase.
    f_menu143 = models.BooleanField(db_column='F_MENU143', blank=True, null=True)  # Field name made lowercase.
    f_menu144 = models.BooleanField(db_column='F_MENU144', blank=True, null=True)  # Field name made lowercase.
    f_menu145 = models.BooleanField(db_column='F_MENU145', blank=True, null=True)  # Field name made lowercase.
    f_menu146 = models.BooleanField(db_column='F_MENU146', blank=True, null=True)  # Field name made lowercase.
    f_menu147 = models.BooleanField(db_column='F_MENU147', blank=True, null=True)  # Field name made lowercase.
    f_menu148 = models.BooleanField(db_column='F_MENU148', blank=True, null=True)  # Field name made lowercase.
    f_menu149 = models.BooleanField(db_column='F_MENU149', blank=True, null=True)  # Field name made lowercase.
    f_menu150 = models.BooleanField(db_column='F_MENU150', blank=True, null=True)  # Field name made lowercase.
    e_cmp = models.CharField(db_column='E_CMP', max_length=3, blank=True, null=True)  # Field name made lowercase.
    n_pin = models.CharField(db_column='N_PIN', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'A_USERS'


class AuthGroup(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.IntegerField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    id = models.IntegerField(primary_key=True)
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class AuthtokenToken(models.Model):
    key = models.CharField(primary_key=True, max_length=40)
    created = models.DateTimeField()
    user = models.OneToOneField(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'authtoken_token'


class DjangoAdminLog(models.Model):
    id = models.IntegerField(primary_key=True)
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    id = models.IntegerField(primary_key=True)
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.IntegerField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class GlobalGobal(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    email = models.CharField(max_length=50, blank=True, null=True)
    created = models.DateField(blank=True, null=True)
    password = models.CharField(max_length=200, blank=True, null=True)
    group = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'global_gobal'


class Session(models.Model):
    session_id = models.CharField(primary_key=True, max_length=50)
    ip_address = models.CharField(max_length=16, blank=True, null=True)
    user_agent = models.CharField(max_length=300, blank=True, null=True)
    user_data = models.TextField(blank=True, null=True)
    last_activity = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'session'


class SessionItem(models.Model):
    id = models.TextField(primary_key=True)
    user_id = models.CharField(max_length=50, blank=True, null=True)
    waktu = models.DateTimeField()
    area = models.CharField(max_length=-1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'session_item'
        unique_together = (('id', 'waktu'),)


class TmOrder(models.Model):
    order_date = models.DateField(blank=True, null=True)
    order_days = models.CharField(max_length=10, blank=True, null=True)
    order_item = models.CharField(max_length=50, blank=True, null=True)
    order_amount = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    client_id = models.CharField(max_length=6, blank=True, null=True)
    client_name = models.CharField(max_length=100, blank=True, null=True)
    client_email = models.CharField(max_length=50, blank=True, null=True)
    client_phone = models.CharField(max_length=20, blank=True, null=True)
    employee = models.ForeignKey('TrEmployee', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tm_order'


class TrEmployee(models.Model):
    employee_id = models.CharField(primary_key=True, max_length=5)
    employee_name = models.CharField(max_length=150, blank=True, null=True)
    employee_email = models.CharField(max_length=50, blank=True, null=True)
    employee_phone = models.CharField(max_length=25, blank=True, null=True)
    e_office = models.CharField(max_length=50, blank=True, null=True)
    s003 = models.CharField(max_length=4, blank=True, null=True)
    boris_baker = models.CharField(db_column='Boris Baker', max_length=16, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    boris_baker_office_com = models.CharField(db_column='boris.baker@office.com', max_length=27, blank=True, null=True)  # Field renamed to remove unsuitable characters.
    field_62_866_541_918 = models.CharField(db_column='+62 866-541-918', max_length=18, blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    jakarta = models.CharField(max_length=7, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tr_employee'
