from django.urls import include, path
from rest_framework.routers import DefaultRouter
from .views import DashTotalSalesList, DashTotalIncomeList, DashGrafikList, RecentOrderList

urlpatterns = [
    path('dashtotalsales/', DashTotalSalesList.as_view()),
    path('dashtotalincome/', DashTotalIncomeList.as_view()),
    path('dashgrafik/', DashGrafikList.as_view()),
    path('recentorder/', RecentOrderList.as_view())
]
