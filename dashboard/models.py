from django.db import models

class DashTotalSales(models.Model):

    totalsales = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_dashsales'


class DashTotalIncome(models.Model):

    totalincome = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_dashincome'


class DashGrafik(models.Model):
    month = models.CharField(max_length=4, blank=True, null=True)
    totalincome = models.DecimalField(max_digits=15, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_dashgr'

class RecentOrder(models.Model):
#    id = models.IntegerField(blank=True, null=True)
    order_date = models.DateField(blank=True, null=True)
    order_days = models.CharField(max_length=10, blank=True, null=True)
    order_item = models.CharField(max_length=50, blank=True, null=True)
    order_amount = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    client_id = models.CharField(max_length=6, blank=True, null=True)
    client_name = models.CharField(max_length=100, blank=True, null=True)
    client_email = models.CharField(max_length=50, blank=True, null=True)
    client_phone = models.CharField(max_length=20, blank=True, null=True)
    employee_id = models.CharField(max_length=4, blank=True, null=True)
    employee_name = models.CharField(max_length=150, blank=True, null=True)
    e_office = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_recentorder'
