import django_filters
from django_filters import BaseInFilter, NumberFilter, CharFilter
from django_filters.rest_framework import DjangoFilterBackend
from django.shortcuts import render
from django.db.models import Count
from .models import DashTotalSales, DashTotalIncome, DashGrafik, RecentOrder
from rest_framework import generics
from .serializers import DashTotalSalesSerializer, DashTotalIncomeSerializer, DashGrafikSerializer, RecentOrderSerializer
from rest_framework.permissions import IsAuthenticated  # <-- Here
from rest_framework import filters
from rest_framework import status
from rest_framework import response
from rest_framework import viewsets
import datetime


class DashTotalSalesList(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = DashTotalSales.objects.all()
    serializer_class = DashTotalSalesSerializer


class DashTotalIncomeList(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = DashTotalIncome.objects.all()
    serializer_class = DashTotalIncomeSerializer

class DashGrafikList(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = DashGrafik.objects.all()
    serializer_class = DashGrafikSerializer

class RecentOrderList(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = RecentOrder.objects.all()
    serializer_class = RecentOrderSerializer
