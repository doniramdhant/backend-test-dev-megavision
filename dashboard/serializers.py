from numpy import source
from rest_framework import serializers
from sqlalchemy import ForeignKey
from .models import DashTotalSales, DashTotalIncome, DashGrafik, RecentOrder

class DashTotalSalesSerializer(serializers.ModelSerializer):
    class Meta:
        model=DashTotalSales
        fields='__all__'

class DashTotalIncomeSerializer(serializers.ModelSerializer):
    class Meta:
        model=DashTotalIncome
        fields='__all__'

class DashGrafikSerializer(serializers.ModelSerializer):
    class Meta:
        model=DashGrafik
        fields='__all__'

class RecentOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model=RecentOrder
        fields='__all__'
