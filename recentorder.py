# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class VRecentorder(models.Model):
    id = models.IntegerField(blank=True, null=True)
    order_date = models.DateField(blank=True, null=True)
    order_days = models.CharField(max_length=10, blank=True, null=True)
    order_item = models.CharField(max_length=50, blank=True, null=True)
    order_amount = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    client_id = models.CharField(max_length=6, blank=True, null=True)
    client_name = models.CharField(max_length=100, blank=True, null=True)
    client_email = models.CharField(max_length=50, blank=True, null=True)
    client_phone = models.CharField(max_length=20, blank=True, null=True)
    employee_id = models.CharField(max_length=4, blank=True, null=True)
    employee_name = models.CharField(max_length=150, blank=True, null=True)
    e_office = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_recentorder'
