from django.shortcuts import render
from .models import Global
from rest_framework import generics
from .serializers import GlobalSerializer
from rest_framework import filters

class CustomerCreate(generics.CreateAPIView):
    # API endpoint that allows creation of a new customer
    queryset = Global.objects.all(),
    serializer_class = GlobalSerializer


class CustomerList(generics.ListAPIView):
    # API endpoint that allows customer to be viewed.
    queryset = Global.objects.all()
    serializer_class = GlobalSerializer
    search_fields = ['name','password']
    filter_backends = (filters.SearchFilter,)

class CustomerDetail(generics.RetrieveAPIView):
    # API endpoint that returns a single customer by pk.
    queryset = Global.objects.all()
    serializer_class = GlobalSerializer

class CustomerUpdate(generics.RetrieveUpdateAPIView):
    # API endpoint that allows a customer record to be updated.
    queryset = Global.objects.all()
    serializer_class = GlobalSerializer

class CustomerDelete(generics.RetrieveDestroyAPIView):
    # API endpoint that allows a customer record to be deleted.
    queryset = Global.objects.all()
    serializer_class = GlobalSerializer
