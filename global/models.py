from django.db import models

class Global(models.Model):
    id = models.AutoField(db_column="id", primary_key=True)  # Field name made lowercase.
    name = models.CharField("Name", max_length=240)
    email = models.EmailField()
    created = models.DateField(auto_now_add=True)
    password = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'global_global'

